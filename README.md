# Bibliothèque (Novembre 2019 - Décembre 2019)

_Ce projet scolaire est une succession de TP pour le module M1103 : Structures de données et algorithmes fondamentaux. Cette solution est une application de gestion de bibliothèque. Le but de ce logiciel était de nous faire découvrir les tableaux, les structures et les algorithmes de trie._

## Comment utiliser le projet ?

Installer Visual Studio 2019 : [https://visualstudio.microsoft.com/fr/downloads/](https://visualstudio.microsoft.com/fr/downloads/)
Et ensuite une fois le projet installé et le projet ouvert dans Visual Studio, vous avez juste à l'utiliser

## Etat du projet 

Fini

## Langages

**FrontEnd :** C / C++

## Auteurs

Hadrien POUZOL / Maxime NGUYEN


