#include <iostream>
#include <string>
#include "Bibliotheque.h"
using namespace std;

int main() {

	int choix;
	t_biblio biblio;
	biblio.nombre = 0;
	biblio.nbrAut = 0;

	do {
		system("CLS");
		cout << endl << endl << endl << "               BIBLIOTHEQUE "<<endl;
		cout << " =========================================" << endl;
		cout << " |                 MENU                  |" << endl;
		cout << " |1-Afficher l ensemble des livres       |" << endl;
		cout << " |2-Ajouter un nouveau livre             |" << endl;
		cout << " |3-Supprimer un livre                   |" << endl;
		cout << " |4-Rechercher un livre                  |" << endl;
		cout << " |5-Livre en double                      |" << endl;
		cout << " |6-Classer par ordre alphabetique       |" << endl;
		cout << " |7-Quitter                              |" << endl;
		cout << " =========================================" << endl << endl;
		cout << "             Choix : ";
		cin >> choix;
		system("CLS");

		switch (choix) {
		case 1:
			viderCin();
			Afficher(biblio);
			break;
		case 2: 
			viderCin();
			Ajout_liv(biblio);
			break;
		case 3:
			viderCin();
			if (biblio.nombre != 0) {
				Sup_liv(biblio);
			}
			else {
				cout << "Il y a pas de livre dans la bibliotheque" << endl << endl;
				system("PAUSE");
			}
			break;
		case 4:
			viderCin();
			if (biblio.nombre != 0) {
			Rech_liv(biblio);
			}
			else {
				cout << "Il y a pas de livre dans la bibliotheque" << endl << endl;
				system("PAUSE");
			}
			break;
		case 5:
			viderCin();
			if (biblio.nombre != 0) {
				Ensemble(biblio);
			}
			else {
				cout << "Il y a pas de livre dans la bibliotheque"<<endl<<endl;
				system("PAUSE");
			}
			break;
		case 6:
		viderCin();
			if (biblio.nombre != 0) {
				Tri(biblio);
			}
			else {
				cout << "Il y a pas de livre dans la bibliotheque" << endl << endl;
				system("PAUSE");
			}
			break;
		case 7: 
			viderCin(); 
			cout << "Au revoir" <<endl<<endl;
			break;
		default:
			viderCin();
			cout << "ERREUR de saisie" << endl<<endl<<endl;
			system("PAUSE");
		}
	} while (choix != 7);
}