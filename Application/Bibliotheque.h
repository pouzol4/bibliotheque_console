#ifndef Bibliotheque
#define Bibliotheque
using namespace std;

int const MAX = 100;

typedef struct {
	string nom;
	string prenom;
	string natio;
	int an_nai;
	int an_dec;
	bool deces;
}t_auteur;

typedef struct {
	string titre;
	int aut;
	int an_parup;
	int nbr_pages;
}t_livre;

typedef struct {
	t_livre livre[MAX];
	int nombre;
	t_auteur auteur[MAX];
	int nbrAut;
}t_biblio;

///////////////////////////////////////////////////////
//
//utilit� : Afficher les livres : titre, nom de l'auteur,
//date de parution et l'ann�e de publication
//
//entr�e : la bibliotheque
//sortie : 
//
///////////////////////////////////////////////////////
void Afficher(t_biblio);

///////////////////////////////////////////////////////
//
//utilit� : Saisir un nouveau livre : titre, nom de l'auteur,
//date de parution et l'ann�e de publication
//
//entr�e : la bibliotheque
//sortie : nombre de livre
//
///////////////////////////////////////////////////////
void Ajout_liv(t_biblio&);

///////////////////////////////////////////////////////
//
//utilit� : Supprimer un livre d�fini par son titre, le nom
//de l'auteur et date de parution
//
//entr�e : la bibliotheque
//sortie : nombre de livre
//
///////////////////////////////////////////////////////
void Sup_liv(t_biblio&);

///////////////////////////////////////////////////////
//
//utilit� : Recherche du livre
//
//entr�e :la bibliotheque
//sortie : 
//
///////////////////////////////////////////////////////
void Rech_liv(t_biblio);

///////////////////////////////////////////////////////
//
//utilit� : V�rifier si la bibliotheque est un ensemble
//si non afficher les doublons
//
//entr�e : la bibliotheque
//sortie : 
//
///////////////////////////////////////////////////////
void Ensemble(t_biblio);

///////////////////////////////////////////////////////
//
//utilit� : tri� la biblioth�que par ordre alphab�tique
//d'abord par rapport au titre, puis l'auteur et puis la
//date de publication
//
//entr�e : la bibliotheque
//sortie : tableau tri�
//
///////////////////////////////////////////////////////
void Tri(t_biblio&);

///////////////////////////////////////////////////////
//
//utilit� :Faire un clear en cas de beug de systeme
//
//entr�e : 
//sortie : 
//
///////////////////////////////////////////////////////
void viderCin();

///////////////////////////////////////////////////////
//
//utilit� :Transformer un mot en majuscule
//
//entr�e : mot
//sortie : mot en majuscule
//
///////////////////////////////////////////////////////
string maj(string);

///////////////////////////////////////////////////////
//
//utilit� :Afficher info auteur � une position d�fini
//au programme
//
//entr�e : la bibliotheque et position auteur
//sortie : 
//
///////////////////////////////////////////////////////
void Aff_aut(t_biblio, int);
#endif // !Bibliotheque