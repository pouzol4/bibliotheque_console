#include <iostream>
#include <string>
#include "Bibliotheque.h"
using namespace std;

void Afficher(t_biblio biblio) {
	int i;

	//Cas pour 0 livre dans la biblioth�que
	if (biblio.nombre == 0) {
		cout << "Il n'y a pas de livre dans la bibliotheque" << endl;
	}
	else {
		cout << "=========================================" << endl;
		cout << "|               AFFICHAGE                " << endl;
		//boucle qui montre les livres
		for (i = 0; i <= biblio.nombre - 1; i++) {
			cout << "|Livre " << i + 1 << endl;
			cout << " |Titre : " << biblio.livre[i].titre << endl;
			cout << " |Auteur : "; 
			Aff_aut (biblio, biblio.livre[i].aut);
			cout << endl;
			cout << " |Annee de parution : " << biblio.livre[i].an_parup << endl;
			cout << " |Nombre de pages : " << biblio.livre[i].nbr_pages << endl;
			cout<<"|"<< endl;
		}

		cout << "=========================================" << endl << endl;
	}
	cout << endl << endl;
	system("PAUSE");
}

void Ajout_liv(t_biblio& biblio) {
	int i, choix;
	char verif;
	choix = -1;
	//v�rification que la biblioth�que est pas pleine
	if (biblio.nombre < MAX) {
		cout << "=========================================" << endl;
		cout << "|                   AJOUT                " << endl;
		cout << "|Titre : ";
		getline(cin, biblio.livre[biblio.nombre].titre);

		if (biblio.nbrAut != 0) {
			cout << "=========================================" << endl;
			cout << " |Auteur : " << endl;
			cout << " |"<<endl;
			for (i = 0; i < biblio.nbrAut ; i++) {
				cout << " |"<<i + 1 << "- ";
				Aff_aut(biblio, i);
				cout << endl;
			}
		
			cout << " |       Choix : ";
			cin >> choix;
		}
		if ((choix <= biblio.nbrAut) && (choix > 0) && biblio.nbrAut != 0) {
			biblio.livre[biblio.nombre].aut = choix-1;
		}
		else {
			cout << "=========================================" << endl;
			cout << "   |                  AUTEUR                " << endl;
			cout << "   |Nom : ";
			cin >> biblio.auteur[biblio.nbrAut].nom;
			biblio.auteur[biblio.nbrAut].nom = maj(biblio.auteur[biblio.nbrAut].nom);
			cout << "   |Prenom : ";
			cin >> biblio.auteur[biblio.nbrAut].prenom;
			cout << "   |Nationalite : ";
			cin >> biblio.auteur[biblio.nbrAut].natio;
			biblio.auteur[biblio.nbrAut].natio = maj(biblio.auteur[biblio.nbrAut].natio);
			cout << "   |Annee de naissance : ";
			cin >> biblio.auteur[biblio.nbrAut].an_nai;
			viderCin();
			cout << "   |Decede ? (o/autre caractere) ";
			cin >> verif;
			viderCin();
			
			if (verif == 'o') {
				biblio.auteur[biblio.nbrAut].deces = false;
				cout << "   |Annee de deces : ";
				cin >> biblio.auteur[biblio.nbrAut].an_dec;
				viderCin();
				while (biblio.auteur[biblio.nbrAut].an_dec < biblio.auteur[biblio.nbrAut].an_nai) {
					cout << "   |" << endl;
					cout << "   |Erreur dans les dates" << endl;

					cout << "   |Annee de naissance : ";
					cin >> biblio.auteur[biblio.nbrAut].an_nai;
					viderCin();
					cout << "   |Decede ? (o/autre caractere) ";
					cin >> verif;
					viderCin();

					if (verif == 'o') {
						biblio.auteur[biblio.nbrAut].deces = false;
						cout << "   |Un auteur a de 10 � 150 ans � sa mort";
						cout << "   |Annee de deces : ";
						cin >> biblio.auteur[biblio.nbrAut].an_dec;
						viderCin();

						while (biblio.auteur[biblio.nbrAut].an_dec <= biblio.auteur[biblio.nbrAut].an_nai + 10 || biblio.auteur[biblio.nbrAut].an_dec > biblio.auteur[biblio.nbrAut].an_nai + 150) {
							cout << "   |Impossible, annee de deces : ";
							cin >> biblio.auteur[biblio.nbrAut].an_dec;
							viderCin();
						}
					}
				}
			}
			biblio.livre[biblio.nombre].aut = biblio.nbrAut;
			biblio.nbrAut = biblio.nbrAut + 1;

		}
		cout << "=========================================" << endl;
		cout << "|Annee de parution : ";
		cin >> biblio.livre[biblio.nombre].an_parup;
		viderCin();
		while (biblio.livre[biblio.nombre].an_parup < biblio.auteur[biblio.livre[biblio.nombre].aut].an_nai+10) {
			cout << "|Impossible, annee de parution : ";
			cin >> biblio.livre[biblio.nombre].an_parup;
			viderCin();
		}
		cout << "|Nombre de page : ";
		cin >> biblio.livre[biblio.nombre].nbr_pages;
		viderCin();
		while (biblio.livre[biblio.nombre].nbr_pages <= 0) {
			cout << "|Erreur de saisi, Nombre de page : ";
			cin >> biblio.livre[biblio.nombre].nbr_pages;
			viderCin();
		}

		cout << "=========================================" << endl << endl;
		biblio.nombre = biblio.nombre + 1;
	}
	else {
		cout << "Bibliotheque pleine" << endl;
	}
	cout << endl << endl;
	system("PAUSE");
}

void Rech_liv(t_biblio biblio) {
	int i;
	int tab[MAX], x;
	string tit;

	//Saisi des propri�t� du livre
	cout << "=========================================" << endl;
	cout << "|               RECHERCHER               " << endl;
	cout << "|Rechercher : ";
	getline(cin, tit);
	cout << "=========================================" << endl << endl;

	x = 0;

	//Recherche du livre
	for (i = 0; i <= biblio.nombre-1;i++) {
		if (maj(tit) == maj(biblio.livre[i].titre)) {
			tab[x] = i;
			x = x + 1;
		}
	}

	//R�sultat de la recherche
	if (x <= 0) {
		cout << "     Erreur. Livre Inexistant" << endl;
	}
	else {
		for (i = 0; i < x; i++) {
			cout << tab[i] + 1 << endl;
			cout << "Titre : " << biblio.livre[tab[i]].titre << endl;
			cout << "Auteur : ";
			Aff_aut(biblio, biblio.livre[tab[i]].aut);
			cout << endl;
			cout << "-----------------------------------------"<<endl;
		}
	}
	cout << endl << endl;
	system("PAUSE");
}

void Sup_liv(t_biblio& biblio) {
	int i, j, pos;
	int tab[MAX], x;
	string tit;

	//Saisi des propri�t� du livre
	cout << "=========================================" << endl;
	cout << "|               SUPPRIMER                " << endl;
	cout << "|Rechercher : ";
	getline(cin, tit);
	cout << "=========================================" << endl << endl;

	x = 0;

	//Recherche du livre
	for (i = 0; i <= biblio.nombre - 1; i++) {
		if (maj(tit) == maj(biblio.livre[i].titre)) {
			tab[x] = i;
			x = x + 1;
		}
	}

	//R�sultat de la recherche
	if (x <= 0) {
		cout << "     Erreur. Livre Inexistant" << endl;
	}
	else {
		for (i = 0; i < x; i++) {
			cout << tab[i] + 1 << endl;
			cout << "Titre : " << biblio.livre[tab[i]].titre << endl;
			cout << "Auteur : ";
			Aff_aut(biblio, biblio.livre[tab[i]].aut);
			cout << endl;
			cout << "-----------------------------------------" << endl;
		}

		cout << endl << "Position du livre a supprimer : ";
		cin >> pos;

		i = 0;

		while (i < x) {
			if (tab[i] +1 == pos) {
				for (j = pos - 1; j < biblio.nombre; j++) {
					biblio.livre[j].titre = biblio.livre[j+1].titre;
					biblio.livre[j].aut = biblio.livre[j + 1].aut;
					biblio.livre[j].an_parup = biblio.livre[j + 1].an_parup;
					biblio.livre[j].nbr_pages = biblio.livre[j + 1].nbr_pages;
				}
				biblio.nombre = biblio.nombre - 1;

				cout << "livre supprimer avec succes";
			}
			i = i + 1;
		}
	}
	cout << endl << endl;
	system("PAUSE");
}


void Ensemble(t_biblio biblio) {
	int i, j, x;
	x = 0;
	i = 0;

	cout << " =========================================" << endl;
	cout << " |               DOUBLONS                 " << endl;
	while (i != biblio.nombre-1) {
		for (j = i+1; j <= biblio.nombre - 1; j++) {
			if ((biblio.livre[i].titre == biblio.livre[j].titre) && (biblio.livre[i].aut == biblio.livre[j].aut) && (biblio.livre[i].an_parup == biblio.livre[j].an_parup) && (biblio.livre[i].nbr_pages == biblio.livre[j].nbr_pages)) {
				x = x + 1;
				cout << " |Titre : " << biblio.livre[i].titre << endl;
				cout << " |Auteur : ";
				Aff_aut(biblio, biblio.livre[i].aut);
				cout << endl;
				cout << " |Annee de parution : " << biblio.livre[i].an_parup << endl;
				cout << " |" << endl;
			}
		}
		i = i + 1;
	}

	if (x == 0) {
		cout << " |            0 livre trouve              " << endl;
	}

	cout << " ========================================="<< endl << endl;

	system("PAUSE");
}

void Tri(t_biblio& biblio) {
	int i, P;
	bool exg;
	string tempo;
	int tempo2;

	P = biblio.nombre - 1;
	exg = true;

	while (exg && P > 0) {
		exg = false;
		for (i = 0; i <= P - 1; i++) {
			if (biblio.livre[i].titre > biblio.livre[i + 1].titre) {
				tempo = biblio.livre[i].titre;
				biblio.livre[i].titre = biblio.livre[i + 1].titre;
				biblio.livre[i + 1].titre = tempo;
				
				tempo2 = biblio.livre[i].aut;
				biblio.livre[i].aut = biblio.livre[i+1].aut;
				biblio.livre[i+1].aut = tempo2;

				tempo2 = biblio.livre[i].an_parup;
				biblio.livre[i].an_parup = biblio.livre[i + 1].an_parup;
				biblio.livre[i + 1].an_parup = tempo2;

				tempo2 = biblio.livre[i].nbr_pages;
				biblio.livre[i].nbr_pages = biblio.livre[i + 1].nbr_pages;
				biblio.livre[i + 1].nbr_pages = tempo2;
				exg = true;
			}

		}
		P = P - 1;
	}
	cout << "Tri effectue avec succes" << endl << endl;
	system("PAUSE");
}

void viderCin() {
	cin.clear();
	cin.seekg(0, ios::end);

	if (!cin.fail()) {
		cin.ignore(numeric_limits<streamsize>::max());
	} else {
		cin.clear();
	}
}

string maj(string nom){
	int i, Nblettre;
	Nblettre = nom.length();

	//passage par une boucle de toutes les lettres du nom
	for (i = 0; i <= Nblettre - 1; i++) {
		//v�rification du caract�re �tant une lettre
		if ((nom[i] >= 'a') && (nom[i] <= 'z')) {
			//transformation du nom en majuscule 
			nom[i] = nom[i] - 'a' + 'A';
		}
	}
	return nom;
}

void Aff_aut(t_biblio biblio, int j) {
	cout << biblio.auteur[j].nom << " ";
	cout << biblio.auteur[j].prenom << ", ";
	cout << biblio.auteur[j].natio << " ne en ";
	cout << biblio.auteur[j].an_nai;

	if (!biblio.auteur[j].deces) {
		cout <<" et mort en " << biblio.auteur[j].an_dec;
	}
}